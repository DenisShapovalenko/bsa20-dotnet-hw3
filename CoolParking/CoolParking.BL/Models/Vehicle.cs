﻿using CoolParking.BL.Core;
using System;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }
        public Vehicle()
        {

        }

        public Vehicle(string id, VehicleType vehicleType, decimal startBalance)
        {
            if (startBalance < 0)
                throw new ArgumentException("The balance should be positive", "startBalance");
            if (!CheckIdFormat(id))
                throw new ArgumentException("Wrong ID format", "id");
            Id = id;
            VehicleType = vehicleType;
            Balance = startBalance;
        }

        
        public static string GenerateRandomRegistrationPlateNumber()
        {
            StringBuilder randomIdBuilder = new StringBuilder(10);
            randomIdBuilder.Append(StringService.RandomString(2));
            randomIdBuilder.Append('-');
            randomIdBuilder.Append(StringService.RandomNumberString(4));
            randomIdBuilder.Append('-');
            randomIdBuilder.Append(StringService.RandomString(2));

            return randomIdBuilder.ToString();
        }

        public string GetLine() => $"{Id} {VehicleType} {Balance}";

        public static bool CheckIdFormat(string id)
        {
            Regex reg = new Regex(@"^[A-Z]{2}-(\d{4})-([A-Z]{2})?");
            return reg.IsMatch(id);
        }
    }
}