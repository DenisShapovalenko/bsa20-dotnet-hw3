﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    public class TopUpVehicleParam
    {
        public string Id { get; set; }
        public decimal Sum { get; set; }
    }
}
