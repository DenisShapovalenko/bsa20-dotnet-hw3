﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking _instance;

        public decimal Balance { get; set; }
        public List<Vehicle> Vehicles { get; set; }
        public int Capacity { get; private set; }

        private Parking() { Capacity = Settings.ParkingCapacity; Vehicles = new List<Vehicle>(); }

        public static Parking InitParking()
        {
            if (_instance == null)
                _instance = new Parking();
            return _instance;
        }

        internal void DropInstance()
        {
            _instance = null;
        }
    }
}