﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private ITimerService _withdrawTimer;
        private ITimerService _logTimer;
        private ILogService _logService;
        private Parking _parking;
        private List<TransactionInfo> _currentTransactions;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Elapsed += WithdrowTimerElapsed;
            _logTimer = logTimer;
            _logTimer.Elapsed += LogTimerElapsed;
            _logService = logService;
            _parking = Parking.InitParking();
            _currentTransactions = new List<TransactionInfo>();
            _withdrawTimer.Start();
            _logTimer.Start();
        }

        private void LogTimerElapsed(object sender, ElapsedEventArgs e)
        {
            WriteCurrentLog();
        }

        private void WriteCurrentLog()
        {
            if (_currentTransactions.Count == 0)
            {
                _logService.Write($"{DateTime.Now:yyyy-MM-dd HH:mm:ss} not yet transactions");
            }
            else
            {
                foreach (TransactionInfo ti in _currentTransactions)
                    _logService.Write(ti.GetLine());
                _currentTransactions.Clear();
            }
        }

        private void WithdrowTimerElapsed(object sender, ElapsedEventArgs e)
        {
            ProcessParking();
        }

        private void ProcessParking()
        {
            foreach (var vehicle in _parking.Vehicles)
                AddTransactionToLog(DateTime.Now, vehicle.Id, ProcessCurrentVehicle(vehicle));
        }

        private decimal ProcessCurrentVehicle(Vehicle vehicle)
        {
            decimal vehiclePaySum = GetSumForVehicle(vehicle);
            vehicle.Balance -= vehiclePaySum;
            _parking.Balance += vehiclePaySum;
            return vehiclePaySum;
        }

        private decimal GetSumForVehicle(Vehicle vehicle)
        {
            decimal tarif;
            if (Settings.Tarifs.TryGetValue(vehicle.VehicleType, out tarif))
            {
                decimal insufficientAmount = Math.Max(0, tarif - Math.Max(0, vehicle.Balance));
                tarif = insufficientAmount * Settings.PenaltyRatio + tarif - insufficientAmount;
            }
            else
                throw new InvalidOperationException($"VehicleType not exist in settings: {vehicle.VehicleType}");
            return tarif;
        }

        private void AddTransactionToLog(DateTime operationDateTime, string vehicleId, decimal vehiclePaySum) =>
            _currentTransactions.Add(new TransactionInfo(operationDateTime, vehicleId, vehiclePaySum));

        public void AddVehicle(Vehicle vehicle)
        {
            Vehicle v = _parking.Vehicles.Find(v => v.Id == vehicle.Id);
            if (v != null)
                throw new ArgumentException("Vehicle with sime ID already exists");
            if (GetFreePlaces() > 0)
                _parking.Vehicles.Add(vehicle);
            else
                throw new InvalidOperationException("No free places");
        }

        public void Dispose()
        {
            _parking.DropInstance();
        }

        public decimal GetBalance() => _parking.Balance;

        public int GetCapacity() => _parking.Capacity;

        public int GetFreePlaces() => _parking.Capacity - _parking.Vehicles.Count;

        public TransactionInfo[] GetLastParkingTransactions() => _currentTransactions.ToArray();

        public ReadOnlyCollection<Vehicle> GetVehicles() => new ReadOnlyCollection<Vehicle>(_parking.Vehicles);

        public string ReadFromLog() => _logService.Read();

        public void RemoveVehicle(string vehicleId)
        {
            Vehicle v = _parking.Vehicles.Find(v => v.Id == vehicleId);
            if (v != null)
            {
                if (v.Balance >= 0)
                    _parking.Vehicles.Remove(v);
                else
                    throw new InvalidOperationException("You can`t remove vehicle with negative balance");
            }
            else
                throw new ArgumentException("Vehicle not found on parking", "vehicleId");
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum > 0)
            {
                Vehicle v = _parking.Vehicles.Find(v => v.Id == vehicleId);
                if (v != null)
                    v.Balance += sum;
                else
                    throw new ArgumentException("Vehicle not found on parking", "vehicleId");
            }
            else
                throw new ArgumentException("Top-up must be greater than zero", "sum");
        }
    }
}