﻿using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private string _logFilePath;

        public LogService(string logFilePath)
        {
            _logFilePath = logFilePath;
        }

        public string LogPath => _logFilePath;

        public string Read()
        {
            if (string.IsNullOrEmpty(LogPath) || !File.Exists(LogPath))
                throw new InvalidOperationException("Logfile don`t found: " + LogPath);
            string retValue = string.Empty;
            using (StreamReader logFileReader = new StreamReader(LogPath))
                retValue = logFileReader.ReadToEnd();
            return retValue;
        }

        public void Write(string logInfo)
        {
            if (string.IsNullOrEmpty(LogPath))
                throw new InvalidOperationException("You need to set filename");
            using (StreamWriter sw = new StreamWriter(LogPath, true))
                sw.WriteLine(logInfo);
        }
    }
}