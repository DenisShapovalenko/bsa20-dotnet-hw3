﻿using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer _timer;
        private double _interval;
        public double Interval { get { return _interval; } set { _interval = value; _timer.Interval = _interval; } }
        public event ElapsedEventHandler Elapsed;
        public void ElapsedEvent(object source, ElapsedEventArgs e) => Elapsed?.Invoke(this, e);
        
        public TimerService()
        {
            _timer = new Timer();
            _timer.Elapsed += ElapsedEvent;
            _timer.AutoReset = true;
            _timer.Enabled = true;
        }

        public void Dispose() => _timer?.Dispose();

        public void Start() => _timer.Start();

        public void Stop() => _timer.Stop();
    }
}