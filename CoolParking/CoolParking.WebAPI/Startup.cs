using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        private TimerService _withdrawTimer;
        private TimerService _logTimer;
        private LogService _logService;
        private ParkingService _parking;
        readonly string _logFile = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _withdrawTimer = new TimerService();
            _withdrawTimer.Interval = Settings.BillingPeriodInSeconds * 1000;
            _logTimer = new TimerService();
            _logTimer.Interval = Settings.LogingPeriodInSeconds * 1000;
            if (File.Exists(_logFile))
                File.Delete(_logFile);
            _logService = new LogService(_logFile);
            _parking = new ParkingService(_withdrawTimer, _logTimer, _logService);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(options => options.EnableEndpointRouting = false);
            services.AddControllers();
            
            services.AddTransient<IParkingService>(s => _parking);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "api/{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
