using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService _parkingService;

        public ParkingController(IParkingService service)
        {
            _parkingService = service;
        }

        
        //GET api/parking/balance
        [HttpGet]
        public async Task<ActionResult<decimal>> Balance()
        {
            return Ok(await Task.Run(() => _parkingService.GetBalance()));
        }

        // GET api/parking/capacity
        [HttpGet]
        public async Task<ActionResult<int>> Capacity()
        {
            return Ok(await Task.Run(() => _parkingService.GetCapacity()));
        }

        //GET api/parking/freePlaces
        [HttpGet]
        public async Task<ActionResult<int>> FreePlaces()
        {
            return Ok(await Task.Run(() => _parkingService.GetFreePlaces()));
        }
    }
}