using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Core;

namespace CoolParking.WebAPI.Controllers
{
    
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private IParkingService _parkingService;

        public TransactionsController(IParkingService service)
        {
            _parkingService = service;
        }
        
        //GET api/transactions/last
        [HttpGet]
        public async Task<ActionResult<TransactionInfo[]>> Last()
        {
            return Ok(await Task.Run(()=>_parkingService.GetLastParkingTransactions()));
        }
        //GET api/transactions/all
        [HttpGet]
        public async Task<ActionResult<string>> All()
        {
            try
            {
                string res = await Task.Run(() => _parkingService.ReadFromLog());
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        //PUT api/transactions/topUpVehicle
        [HttpPut]
        public async Task<ActionResult<Vehicle>> TopUpVehicle([FromBody]TopUpVehicleParam param)
        {
            if (param == null || param.Sum <= 0M)
                return BadRequest("Body is invalid");
            if (!Vehicle.CheckIdFormat(param.Id))
                return BadRequest("Body is invalid");
            
            var vs = await Task.Run(() => _parkingService.GetVehicles());
            var v = vs.ToArray().FirstOrDefault(vv => vv.Id == param.Id);
            if (v == null)
                return NotFound("Vehicle not found");
            _parkingService.TopUpVehicle(param.Id, param.Sum);
            vs = await Task.Run(() => _parkingService.GetVehicles());
            v = vs.ToArray().FirstOrDefault(vv => vv.Id == param.Id);
            return Ok(v);
        }
    }
}