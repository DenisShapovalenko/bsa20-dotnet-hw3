using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Core;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {
        private IParkingService _parkingService;

        public VehiclesController(IParkingService service)
        {
            _parkingService = service;
        }

        //GET api/vehicles
        [HttpGet]
        public async Task<ActionResult<Vehicle[]>> Get()
        {
            return Ok(await Task.Run(() => _parkingService.GetVehicles().ToArray()));
        }
        //GET api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        [HttpGet("{id}")]
        public async Task<ActionResult<Vehicle>> Get(string id)
        {
            /*If id is invalid - Status Code: 400 Bad Request
            If vehicle not found - Status Code: 404 Not Found*/
            if (!Vehicle.CheckIdFormat(id))
                return BadRequest("ID is invalid");
            var vs = await Task.Run(() => _parkingService.GetVehicles());
            var v = vs.ToArray().First(vv => vv.Id == id);
            if (v == null)
                return NotFound("Vehicle not found");
            return Ok(v);
        }

        //POST api/vehicles
        [HttpPost()]
        public async Task<ActionResult<Vehicle>> Post([FromBody]Vehicle vehicle)
        {
            if (vehicle == null)
            {
                return BadRequest("Body is invalid");
            }
            if (!Vehicle.CheckIdFormat(vehicle.Id))
            {
                return BadRequest("Id is invalid");
            }
            if ((int)vehicle.VehicleType < 0 || (int)vehicle.VehicleType > 3)
            {
                return BadRequest("VehicleType is invalid");
            }
            if (vehicle.Balance <= 0)
            {
                return BadRequest("Balance should be gretter 0");
            }
            if (_parkingService.GetVehicles().First(v => v.Id == vehicle.Id) == null)
            {
                return BadRequest("Vehicle with sime ID already exists");
            }
            try
            {
                _parkingService.AddVehicle(vehicle);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return StatusCode(201, vehicle);
        }

        //DELETE api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            if (!Vehicle.CheckIdFormat(id))
                return BadRequest("ID is invalid");
            var vs = await Task.Run(() => _parkingService.GetVehicles());
            var v = vs.ToArray().First(vv => vv.Id == id);
            if (v == null)
                return NotFound("Vehicle not found");
            await Task.Run(()=>_parkingService.RemoveVehicle(id));
            return NoContent();
        }
    }
}