﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.App
{
    public static class ConnectionSettings
    {
        public static string ConnectionString { get; set; } = "https://localhost:5001/";
    }
}
