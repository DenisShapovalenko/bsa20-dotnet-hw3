﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using System;

namespace CoolParking.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var proc = new Processor();
            proc.Run();
        }

    }
}
