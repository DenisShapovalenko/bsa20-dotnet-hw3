﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.App
{
    public class ParkingWebAPIClient
    {
        private static HttpClient _httpClient = new HttpClient();
        private static async Task<WebClientResponse> GetWebApi(string apiPath)
        {
            using (var result = await _httpClient.GetAsync(GetFullPath(apiPath)))
            {
                return new WebClientResponse { Code = (int)result.StatusCode, Message = result.Content.ReadAsStringAsync().Result };
            }
        }

        private static async Task<WebClientResponse> PostWebApi(string apiPath, string jsonString)
        {
            using (var content = new StringContent(jsonString, Encoding.UTF8, "application/json"))
            {
                using (var result = await _httpClient.PostAsync(GetFullPath(apiPath), content))
                {
                    return new WebClientResponse { Code = (int)result.StatusCode, Message = result.Content.ReadAsStringAsync().Result };
                }
            }
        }

        private static async Task<WebClientResponse> PutWebApi(string apiPath, string jsonString)
        {
            using (var content = new StringContent(jsonString, Encoding.UTF8, "application/json"))
            {
                using (var result = await _httpClient.PutAsync(GetFullPath(apiPath), content))
                {
                    return new WebClientResponse { Code = (int)result.StatusCode, Message = result.Content.ReadAsStringAsync().Result };
                }
            }
        }

        private static async Task<WebClientResponse> DeleteWebApi(string apiPath)
        {
            using (var result = await _httpClient.DeleteAsync(GetFullPath(apiPath)))
            {
                return new WebClientResponse { Code = (int)result.StatusCode, Message = result.Content.ReadAsStringAsync().Result };
            }
        }

        private static string GetFullPath(string apiPath) => ConnectionSettings.ConnectionString + apiPath;

        public static int GetFreePlaces()
        {
            WebClientResponse resp = GetWebApi("api/parking/freePlaces").Result;
            if (resp.Code == 200)
            {
                return JsonConvert.DeserializeObject<int>(resp.Message);
            }
            throw new Exception($"Ошибка запроса данных: {resp.Code} /r/n {resp.Message}");
        }

        public static int GetCapacity()
        {
            WebClientResponse resp = ParkingWebAPIClient.GetWebApi("api/parking/capacity").Result;
            if (resp.Code == 200)
            {
                return JsonConvert.DeserializeObject<int>(resp.Message);
            }
            throw new Exception($"Ошибка запроса данных: {resp.Code} /r/n {resp.Message}");
        }

        public static IEnumerable<Vehicle> GetVehicles()
        {
            WebClientResponse resp = ParkingWebAPIClient.GetWebApi("api/vehicles").Result;
            if (resp.Code == 200)
            {
                return JsonConvert.DeserializeObject<IEnumerable<Vehicle>>(resp.Message);
            }
            throw new Exception($"Ошибка запроса данных: {resp.Code} /r/n {resp.Message}");
        }

        public static Vehicle TopUpVehicle(TopUpVehicleParam param)
        {
            WebClientResponse postResp = ParkingWebAPIClient.PutWebApi("api/transactions/topUpVehicle", JsonConvert.SerializeObject(param)).Result;
            if (postResp.Code != 200)
            {
                throw new Exception($"Ошибка запроса данных: {postResp.Code} /r/n {postResp.Message}");
            }
            return JsonConvert.DeserializeObject<Vehicle>(postResp.Message);
        }

        public static void DeleteVehicle(string newId)
        {
            WebClientResponse postResp = ParkingWebAPIClient.DeleteWebApi($"api/vehicles/{newId}").Result;
            if (postResp.Code != 204)
            {
                throw new Exception($"Ошибка запроса данных: {postResp.Code} /r/n {postResp.Message}");
            }
        }

        public static Vehicle AddVehicle(Vehicle vehicle)
        {
            WebClientResponse postResp = ParkingWebAPIClient.PostWebApi($"api/vehicles", JsonConvert.SerializeObject(vehicle)).Result;
            if (postResp.Code != 201)
            {
                throw new Exception($"Ошибка запроса данных: {postResp.Code} /r/n {postResp.Message}");
            }
            return vehicle;
        }

        public static string GetHistoryTransactions()
        {
            WebClientResponse getResp = ParkingWebAPIClient.GetWebApi($"api/transactions/all").Result;
            if (getResp.Code == 200)
                return getResp.Message;
            throw new Exception($"Ошибка запроса данных: {getResp.Code} /r/n {getResp.Message}");
        }

        public static TransactionInfo[] GetLastTransactions()
        {
            WebClientResponse getResp = ParkingWebAPIClient.GetWebApi($"api/transactions/last").Result;
            if (getResp.Code == 200)
            {
                return JsonConvert.DeserializeObject<IEnumerable<TransactionInfo>>(getResp.Message).ToArray();
            }
            throw new Exception($"Ошибка запроса данных: {getResp.Code} /r/n {getResp.Message}");
        }

        public static decimal GetBalance()
        {
            WebClientResponse getResp = ParkingWebAPIClient.GetWebApi($"api/parking/balance").Result;
            if (getResp.Code == 200)
            {
                return JsonConvert.DeserializeObject<decimal>(getResp.Message);
            }
            throw new Exception($"Ошибка запроса данных: {getResp.Code} /r/n {getResp.Message}");
        }
    }
}
