﻿using CoolParking.BL.Core;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;

namespace CoolParking.App
{
    internal class Processor : IDisposable
    {
        public Processor()
        {
        }
        public void Dispose()
        {
            
        }

        internal void Run()
        {
            while(ProcessMainMenu()) { }
        }

        private bool ProcessMainMenu()
        {
            DisplayMainMenu();
            var x = Console.ReadKey();
            Console.WriteLine(); // перенос курсора после любой клавиши
            switch (x.KeyChar)
            {
                case '0':
                    return false;
                case '1':
                    DisplayCurrentBalance();
                    break;
                case '2':
                    DisplayCurrentEarnings();
                    break;
                case '3':
                    DisplayCurrentPlaces();
                    break;
                case '4':
                    DisplayCurrentTransactions();
                    break;
                case '5':
                    DisplayHistoryTransactions();
                    break;
                case '6':
                    DisplayVehiclesList();
                    break;
                case '7':
                    AddVehicle();
                    break;
                case '8':
                    RemoveVehicle();
                    break;
                case '9':
                    TopUpVehicle();
                    break;
                default:
                    DisplayErrorString("Указанная вами операция отсутствует в списке. Будьте внимательны");
                    break;
            }
            WaitForReturnToMainMenu();
            return true;
        }

        private void DisplayErrorString(string outputString)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(outputString);
            Console.ResetColor();
        }

        private void WaitForReturnToMainMenu()
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Нажмите любую клавишу для возврата в меню");
            Console.ResetColor();
            Console.ReadKey();
        }

        private bool IsParkingEmpty() => ParkingWebAPIClient.GetFreePlaces() == ParkingWebAPIClient.GetCapacity();

        

        private void TopUpVehicle()
        {
            try
            {
                if (IsParkingEmpty())
                {
                    DisplayErrorString("На парковке нет транспортных средств");
                    return;
                }
            }
            catch (Exception ex)
            {
                DisplayErrorString(ex.Message);
                return;
            }
            string newId = ReadVehicleId();
            if (string.IsNullOrEmpty(newId))
                return;
            decimal balance = ReadVehicleBalance();
            if (balance > 0)
            {
                ParkingWebAPIClient.TopUpVehicle(new TopUpVehicleParam { Id = newId, Sum = balance });
            }
        }

        private void RemoveVehicle()
        {
            if (IsParkingEmpty())
            {
                DisplayErrorString("На парковке нет транспортных средств");
                return;
            }
            string newId = ReadVehicleId();
            if (string.IsNullOrEmpty(newId))
                return;
            ParkingWebAPIClient.DeleteVehicle(newId);
            
        }



        private void AddVehicle()
        {
            if (ParkingWebAPIClient.GetFreePlaces() == 0)
            {
                DisplayErrorString("Нет свободных мест");
                return;
            }
            string newId = ReadVehicleId();
            if (string.IsNullOrEmpty(newId))
                return;
            IEnumerable<Vehicle> vehicles = ParkingWebAPIClient.GetVehicles();
            if (vehicles.Any(t => t.Id == newId))
            {
                DisplayErrorString("ТС с таким ID уже стоит на парковке");
                return;
            }
            VehicleType vt = ReadVehicleType();
            decimal balance = ReadVehicleBalance();
            if (balance > 0)
            {
                try
                {
                    ParkingWebAPIClient.AddVehicle(new Vehicle(newId, vt, balance));
                }
                catch(Exception ex)
                {
                    DisplayErrorString(ex.Message);
                }
            }
        }

        private decimal ReadVehicleBalance()
        {
            WriteCaption("Ведите баланс (> 0): ", false);
            string answ = Console.ReadLine();
            decimal balance;
            while (!decimal.TryParse(answ, out balance) || (balance < 0.01M))
            {
                DisplayErrorString("Неверное значение баланса");
                if (!AskYN("Желаете повторить?"))
                {
                    balance = -1;
                    break;
                }
                Console.WriteLine();
                WriteCaption("Ведите баланс (> 0): ", false);
                answ = Console.ReadLine();
            }
            return balance;
        }

        private VehicleType ReadVehicleType()
        {
            WriteCaption("Ведите тип ТС [Легковое - 1, Грузовое - 2, Автобус - 3, Мотоцикл - 4]: ", false);
            string answ = Console.ReadKey().KeyChar.ToString();
            while (!"1234".Contains(answ))
                answ = Console.ReadKey().KeyChar.ToString();
            Console.WriteLine();
            return (VehicleType)(int.Parse(answ)-1);
        }

        private string ReadVehicleId()
        {
            WriteCaption("Введите ID автомобиля [AA-0000-AA]: ", false);
            string newId = Console.ReadLine();
            while (!Vehicle.CheckIdFormat(newId))
            {
                DisplayErrorString("Неверный формат ID");
                if (!AskYN("Желаете повторить?"))
                {
                    newId = string.Empty;
                    break;
                }
                Console.WriteLine();
                WriteCaption("Введите ID автомобиля [AA-0000-AA]: ", false);
                newId = Console.ReadLine();
            }
            return newId;
        }

        private bool AskYN(string text)
        {
            WriteCaption(text + " [Y/N]", false);
            string answ = Console.ReadKey().KeyChar.ToString().ToUpper();
            while (answ != "N" && answ != "Y")
            {
                answ = Console.ReadKey().KeyChar.ToString().ToUpper();
            }
            return answ == "Y";
        }

        private void DisplayVehiclesList()
        {
            try
            {
                var vs = ParkingWebAPIClient.GetVehicles().ToList();
                if (vs.Count == 0)
                {
                    WriteCaption("На парковке", false);
                    WriteValue(" нет автомобилей", true);
                }
                else
                {
                    WriteCaption("На парковке находятся:", true);
                    foreach (var v in vs)
                        WriteValue(v.GetLine(), true);
                }
            }
            catch (Exception ex)
            {
                DisplayErrorString(ex.Message);
            }
        }

        private void DisplayHistoryTransactions()
        {
            WriteCaption("История транзакций:", true);
            try
            {
                try
                {
                    WriteValue(ParkingWebAPIClient.GetHistoryTransactions(), true);
                }
                catch (Exception ex)
                {
                    DisplayErrorString(ex.Message);
                }
            }
            catch (Exception e)
            {
                DisplayErrorString(e.Message);
            }
            
        }

        private void DisplayCurrentTransactions()
        {
            WriteCaption("Текущие платежи:");
            Console.WriteLine();
            try
            {
                TransactionInfo[] ts = ParkingWebAPIClient.GetLastTransactions();

                if (ts.Length == 0)
                {
                    WriteValue("Отсутствуют", true);
                }
                else
                    foreach (var t in ts)
                    {
                        WriteValue(t.GetLine(), true);
                    }
            }
            catch (Exception ex)
            {
                DisplayErrorString(ex.Message);
            }
            
        }

        private void DisplayCurrentPlaces()
        {
            WriteCaption("Свободно/занято мест: ");
            int fp = ParkingWebAPIClient.GetFreePlaces();
            int up = ParkingWebAPIClient.GetCapacity() - fp;
            WriteValue($"{fp} / {up}", true);
        }

        private void DisplayCurrentEarnings()
        {
            WriteCaption("Заработано за текущий период: ");

            WriteValue(ParkingWebAPIClient.GetLastTransactions().Sum(ti => ti.Sum).ToString(), true);
        }

        private void DisplayCurrentBalance()
        {
            WriteCaption("Текущий баланс парковки: ");

            WriteValue(ParkingWebAPIClient.GetBalance().ToString(), true);
        }

        private void WriteValue(string outputValue, bool newLine = true)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(outputValue);
            Console.ResetColor();
            if (newLine)
                Console.WriteLine();
        }

        private void WriteCaption(string outputCaption, bool newLine = false)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(outputCaption);
            Console.ResetColor();
            if (newLine)
                Console.WriteLine();
        }

        private void DisplayMainMenu()
        {
            Console.Clear();
            Console.WriteLine("1 - Текущий баланс парковки");
            Console.WriteLine("2 - Заработано за текущий период (до записи в лог)");
            Console.WriteLine("3 - Количество свободных / занятых мест на парковке");
            Console.WriteLine("4 - Транзакции Парковки за текущий период (до записи в лог)");
            Console.WriteLine("5 - История транзакций");
            Console.WriteLine("6 - Список ТС находящихся на Паркинге");
            Console.WriteLine("7 - Поставить ТС на Паркинг");
            Console.WriteLine("8 - Забрать ТС с Паркинга");
            Console.WriteLine("9 - Пополнить баланс ТС");
            Console.WriteLine("==============================================");
            Console.WriteLine("0 - Завершить работу парковки");
            Console.WriteLine("==============================================");
            Console.WriteLine("");
            Console.Write("Выберите операцию:");
        }
    }
}
