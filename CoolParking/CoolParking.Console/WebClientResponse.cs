﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.App
{
    public class WebClientResponse
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
